/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "maini.h"
#include "mainfunc.h"
#include "support.h"

/////////////////////////////////////////////////////////////////////////////
///////////////////// maini.c - Main Interface //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

GtkWidget * inst_list;
GtkWidget * uninst_list;
GtkWidget * gnome_list;
guint ListSelected_selected_row=-1;

static GnomeUIInfo file1_menu_uiinfo[] =
{
  GNOMEUIINFO_MENU_EXIT_ITEM (on_exit1_activate, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo actions1_menu_uiinfo[] =
{
  {
    GNOME_APP_UI_ITEM, N_("Install Software..."),
    NULL,
    on_install_software1_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CDROM,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Uninstall Software..."),
    NULL,
    on_uninstall_software1_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TRASH,
    0, 0, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM, N_("Filetype Converter..."),
    NULL,
    on_file_type_converter1_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, 0, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM, N_("Update GNOME Software Map"),
    NULL,
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_REFRESH,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Download Selected Software"),
    NULL,
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_JUMP_TO,
    0, 0, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo view1_menu_uiinfo[] =
{
  {
    GNOME_APP_UI_ITEM, N_("Installed Software"),
    NULL,
    on_view_all_software_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Uninstalled Software"),
    NULL,
    on_view_uninstalled_software_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("GNOME Software Map"),
    NULL,
    on_view_gnome_softwaremap_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo settings1_menu_uiinfo[] =
{
  GNOMEUIINFO_MENU_PREFERENCES_ITEM (NULL, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help1_menu_uiinfo[] =
{
  {
    GNOME_APP_UI_ITEM, N_("Help..."),
    NULL,
    on_user_guide1_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BOOK_OPEN,
    0, 0, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_ABOUT_ITEM (on_about1_activate, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo menubar1_uiinfo[] =
{
  GNOMEUIINFO_MENU_FILE_TREE (file1_menu_uiinfo),
  {
    GNOME_APP_UI_SUBTREE, N_("A_ctions"),
    NULL,
    actions1_menu_uiinfo, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, N_("A_ctions"),
    0, 0, NULL
  },
  GNOMEUIINFO_MENU_VIEW_TREE (view1_menu_uiinfo),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings1_menu_uiinfo),
  GNOMEUIINFO_MENU_HELP_TREE (help1_menu_uiinfo),
  GNOMEUIINFO_END
};

GtkWidget*
create_MainWindow ()
{
  GtkWidget* MainWindow;
  GtkWidget* vbox1;
  GtkWidget* handlebox1;
  GtkWidget* menubar1;
  GtkWidget* handlebox2;
  GtkWidget* toolbar1;
  GtkWidget* tmp_toolbar_icon;
  GtkWidget* InstallSoftware_button;
  GtkWidget* UninstallSoftware_button;
  GtkWidget* Update_button;
  GtkWidget* Download_button;
  GtkWidget* Exit_button;
  GtkWidget* vbox2;
  GtkWidget* scrolledwindow3;
  GtkWidget* label13;
  GtkWidget* label14;
  GtkWidget* label15;
  GtkWidget* label16;
  GtkWidget* label17;
  GtkWidget* InstalledSoftware_label;
  GtkWidget* scrolledwindow4;
  GtkWidget* label22;
  GtkWidget* label23;
  GtkWidget* label24;
  GtkWidget* label25;
  GtkWidget* UninstalledSoftware_label;
  GtkWidget* scrolledwindow5;
  GtkWidget* label26;
  GtkWidget* label27;
  GtkWidget* label28;
  GtkWidget* label29;
  GtkWidget* label30;
  GtkWidget* label31;
  GtkWidget* GNOMESoftwareMap_label;
  GtkWidget* appbar1;
  GtkWidget* warnmessage;
  GtkWidget* instoruninst;

  MainWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (MainWindow), "MainWindow", MainWindow);
  gtk_window_set_title (GTK_WINDOW (MainWindow), _("GNOME Software Manager"));
  gtk_signal_connect (GTK_OBJECT (MainWindow), "show",
                      GTK_SIGNAL_FUNC (on_MainWindow_show),
                      NULL);

  if (mwin.xpos != 0 && mwin.ypos != 0)
    gtk_widget_set_uposition (MainWindow, mwin.xpos, mwin.ypos);
  gtk_widget_set_usize (MainWindow, mwin.width, mwin.height);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (MainWindow), vbox1);

  handlebox1 = gtk_handle_box_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), handlebox1, FALSE, TRUE, 0);

  menubar1 = gtk_menu_bar_new ();
  gtk_container_add (GTK_CONTAINER (handlebox1), menubar1);
  gnome_app_fill_menu (GTK_MENU_SHELL (menubar1), menubar1_uiinfo,
                       NULL, FALSE, 0);

  gtk_widget_ref (actions1_menu_uiinfo[2].widget);
  gtk_widget_ref (actions1_menu_uiinfo[4].widget);
  
  gtk_widget_set_sensitive (GTK_WIDGET (actions1_menu_uiinfo[5].widget), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (actions1_menu_uiinfo[6].widget), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (settings1_menu_uiinfo[0].widget), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (help1_menu_uiinfo[0].widget), FALSE);

  gtk_widget_ref (help1_menu_uiinfo[1].widget);  
  gtk_widget_ref (help1_menu_uiinfo[1].widget);

  handlebox2 = gtk_handle_box_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), handlebox2, FALSE, TRUE, 0);

  toolbar1 = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
  gtk_container_add (GTK_CONTAINER (handlebox2), toolbar1);
  gtk_container_set_border_width (GTK_CONTAINER (toolbar1), 2);
  gtk_toolbar_set_space_size (GTK_TOOLBAR (toolbar1), 16);
  gtk_toolbar_set_space_style (GTK_TOOLBAR (toolbar1), GTK_TOOLBAR_SPACE_LINE);
  gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar1), GTK_RELIEF_NONE);
  gtk_toolbar_set_space_size (GTK_TOOLBAR (toolbar1), 16);
  gtk_toolbar_set_space_style (GTK_TOOLBAR (toolbar1), GTK_TOOLBAR_SPACE_LINE);

  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_CDROM);
  InstallSoftware_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Install"),
                                _("Install/Upgrade Software"), NULL,
                                tmp_toolbar_icon, on_install_software1_activate, NULL);
  
  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_TRASH);
  UninstallSoftware_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Uninstall"),
                                _("Uninstall Software"), NULL,
                                tmp_toolbar_icon, on_uninstall_software1_activate, NULL);
  
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar1));

  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_REFRESH);
  Update_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Update"),
                                _("Update GNOME Software Map"), NULL,
                                tmp_toolbar_icon, NULL, NULL);
  gtk_widget_set_sensitive (GTK_WIDGET (Update_button), FALSE);

  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_JUMP_TO);
  Download_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Download"),
                                _("Download Selected Software"), NULL,
                                tmp_toolbar_icon, NULL, NULL);
  gtk_widget_set_sensitive (GTK_WIDGET (Download_button), FALSE);

  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar1));

  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_PREFERENCES);
  Preferences_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Preferences"),
                                _("Preferences"), NULL,
                                tmp_toolbar_icon, on_exit1_activate, NULL);
  
  tmp_toolbar_icon = gnome_stock_pixmap_widget (MainWindow, GNOME_STOCK_PIXMAP_EXIT);
  Exit_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar1),
                                GTK_TOOLBAR_CHILD_BUTTON,
                                NULL,
                                _("Exit"),
                                _("Exit"), NULL,
                                tmp_toolbar_icon, on_exit1_activate, NULL);
  
  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

  GIP_notebook = gtk_notebook_new ();
  gtk_box_pack_start (GTK_BOX (vbox2), GIP_notebook, TRUE, TRUE, 0);

  scrolledwindow3 = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (GIP_notebook), scrolledwindow3);

  inst_list = gtk_clist_new (5);
  //MI
  gtk_signal_connect (GTK_OBJECT (inst_list), "select_row",
                      GTK_SIGNAL_FUNC (on_inst_list_select_row),
		      NULL);
  gtk_container_add (GTK_CONTAINER (scrolledwindow3), inst_list);
  gtk_clist_set_column_width (GTK_CLIST (inst_list), 0, 120);
  gtk_clist_set_column_width (GTK_CLIST (inst_list), 1, 80);
  gtk_clist_set_column_width (GTK_CLIST (inst_list), 2, 80);
  gtk_clist_set_column_width (GTK_CLIST (inst_list), 3, 80);
  gtk_clist_set_column_width (GTK_CLIST (inst_list), 4, 80);
  gtk_clist_column_titles_show (GTK_CLIST (inst_list));
  // gtk_widget_set_usize (inst_list, 700, 350);

  label13 = gtk_label_new (_("Software"));
  gtk_clist_set_column_widget (GTK_CLIST (inst_list), 0, label13);

  label14 = gtk_label_new (_("Version"));
  gtk_clist_set_column_widget (GTK_CLIST (inst_list), 1, label14);

  label15 = gtk_label_new (_("Size (kB)"));
  gtk_clist_set_column_widget (GTK_CLIST (inst_list), 2, label15);

  label16 = gtk_label_new (_("GNOME/KDE"));
  gtk_clist_set_column_widget (GTK_CLIST (inst_list), 3, label16);

  label17 = gtk_label_new (_("Description"));
  gtk_clist_set_column_widget (GTK_CLIST (inst_list), 4, label17);

  InstalledSoftware_label = gtk_label_new (_("Installed Software"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (GIP_notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (GIP_notebook), 0), InstalledSoftware_label);

  scrolledwindow4 = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (GIP_notebook), scrolledwindow4);

  uninst_list = gtk_clist_new (4);
  gtk_container_add (GTK_CONTAINER (scrolledwindow4), uninst_list);
  gtk_clist_set_column_width (GTK_CLIST (uninst_list), 0, 120);
  gtk_clist_set_column_width (GTK_CLIST (uninst_list), 1, 80);
  gtk_clist_set_column_width (GTK_CLIST (uninst_list), 2, 80);
  gtk_clist_set_column_width (GTK_CLIST (uninst_list), 3, 80);
  gtk_clist_column_titles_show (GTK_CLIST (uninst_list));
  // gtk_widget_set_usize (uninst_list, 700, 350);

  label22 = gtk_label_new (_("Software"));
  gtk_clist_set_column_widget (GTK_CLIST (uninst_list), 0, label22);

  label23 = gtk_label_new (_("Version"));
  gtk_clist_set_column_widget (GTK_CLIST (uninst_list), 1, label23);

  label24 = gtk_label_new (_("GNOME/KDE"));
  gtk_clist_set_column_widget (GTK_CLIST (uninst_list), 2, label24);

  label25 = gtk_label_new (_("Description"));
  gtk_clist_set_column_widget (GTK_CLIST (uninst_list), 3, label25);

  UninstalledSoftware_label = gtk_label_new (_("Uninstalled Software"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (GIP_notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (GIP_notebook), 1), UninstalledSoftware_label);

  scrolledwindow5 = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (GIP_notebook), scrolledwindow5);

  gnome_list = gtk_clist_new (6);
  gtk_object_set_data_full (GTK_OBJECT (MainWindow), "gnome_list", gnome_list,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_container_add (GTK_CONTAINER (scrolledwindow5), gnome_list);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 0, 120);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 1, 80);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 2, 80);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 3, 105);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 4, 80);
  gtk_clist_set_column_width (GTK_CLIST (gnome_list), 5, 80);
  gtk_clist_column_titles_show (GTK_CLIST (gnome_list));
  // gtk_widget_set_usize (gnome_list, 700, 350);

  label26 = gtk_label_new (_("Software"));
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 0, label26);

  label27 = gtk_label_new (_("Stable"));
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 1, label27);

  label28 = gtk_label_new (_("Developer"));
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 2, label28);

  label29 = gtk_label_new (_("Size Stable (kB)"));
  gtk_object_set_data_full (GTK_OBJECT (MainWindow), "label29", label29,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 3, label29);

  label30 = gtk_label_new (_("Type"));
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 4, label30);

  label31 = gtk_label_new (_("Description"));
  gtk_clist_set_column_widget (GTK_CLIST (gnome_list), 5, label31);

  GNOMESoftwareMap_label = gtk_label_new (_("GNOME Software Map"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (GIP_notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (GIP_notebook), 2), GNOMESoftwareMap_label);

  appbar1 = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
  gtk_box_pack_start (GTK_BOX (vbox2), appbar1, FALSE, TRUE, 0);

  gtk_widget_show (vbox1);
  gtk_widget_show (handlebox1);
  gtk_widget_show (menubar1); 
  gtk_widget_show (handlebox2);
  gtk_widget_show (toolbar1);
  gtk_widget_show (InstallSoftware_button);
  gtk_widget_show (UninstallSoftware_button);
  gtk_widget_show (Update_button);
  gtk_widget_show (Download_button);
  gtk_widget_show (Preferences_button);
  gtk_widget_show (Exit_button);
  gtk_widget_show (vbox2);
  gtk_widget_show (GIP_notebook);
  gtk_widget_show (scrolledwindow3);
  gtk_widget_show (inst_list);
  gtk_widget_show (label13);
  gtk_widget_show (label14);
  gtk_widget_show (label15);
  gtk_widget_show (label16);
  gtk_widget_show (label17);
  gtk_widget_show (InstalledSoftware_label);
  gtk_widget_show (scrolledwindow4);
  gtk_widget_show (uninst_list);
  gtk_widget_show (label22);
  gtk_widget_show (label23);
  gtk_widget_show (label24);
  gtk_widget_show (label25);
  gtk_widget_show (UninstalledSoftware_label);
  gtk_widget_show (scrolledwindow5);
  gtk_widget_show (gnome_list);
  gtk_widget_show (label26);
  gtk_widget_show (label27);
  gtk_widget_show (label28);
  gtk_widget_show (label29);
  gtk_widget_show (label30);
  gtk_widget_show (label31);
  gtk_widget_show (GNOMESoftwareMap_label);
  gtk_widget_show (appbar1);
 
  // add by MARMORAT Irwin for database management
  DataBase_ReadAll();
  //DataBase_Test();
  
  return MainWindow;
}

GtkWidget*
create_About ()
{
  const gchar* authors[] = {
    "Programming: Andreas Hyd�n <andreas.hyden@telia.com>",
    "                    Marmorat Irwin <mirwinm@yahoo.com>",
    "                    Jay Carlson <jay.carlson@ebox.tninet.se>",
    
    "Graphics: Toussaint Fr�d�ric <ftoussin@club-internet.fr>",
     NULL
  };
  GtkWidget *about, *href, *hbox;

  about = gnome_about_new ("GIP", VERSION,
                        _("Copyright (C) 1999, 2000 Andreas Hyd�n, Marmorat Irwin"),
                        authors,
			   _("GIP stands for the GNOME Install Project, the goal of which is to make installation,\nupgrading, uninstallation and management of software in Linux very easy."),
			"gip/gip-logo.png");
  hbox = gtk_hbox_new (TRUE, 0);
  href = gnome_href_new ("http://gip.darkorb.net",
			       _("GNOME Install Project web site"));
  gtk_box_pack_start (GTK_BOX (hbox), href, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (about)->vbox), hbox, TRUE, FALSE, 0);
  gtk_widget_show_all (GTK_WIDGET (hbox));

  gtk_object_set_data (GTK_OBJECT (about), "about", about);
  gtk_window_set_modal (GTK_WINDOW (about), TRUE);

  return about;
}
