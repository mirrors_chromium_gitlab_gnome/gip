/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// advancedfunc.h - Advanced Function ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void
on_installfilentrychanged_activate			(GtkWidget*		widget,
									 gpointer 		fileentry);

void
on_advancedcancel_activate					(GtkWidget*		widget,
									 gpointer		window);
void
on_advancedbutton_activate					(GtkWidget*		widget,
									 gpointer		user_data);


void
on_advancedsrcokbutton_activate				(GtkWidget*		widget,
									 gpointer		window);

void
on_advancedrpmapplybutton_activate			(GtkWidget*		widget,
									 gpointer		apply_button);

void
on_advancedrpmokbutton_activate				(GtkWidget*		widget,
									 gpointer		window);

void
on_advancedsrcapplybutton_activate			(GtkWidget*		widget,
									 gpointer		window);


void
on_extraentrychanged_activate			(GtkWidget*		widget,
									 gpointer		apply_button);

void
on_makeextraentrychanged_activate			(GtkWidget*		widget,
									 gpointer		apply_button);