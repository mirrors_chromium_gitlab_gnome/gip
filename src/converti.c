/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////// converti.c - Convert Interface ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "converti.h"
#include "convertfunc.h"
#include "support.h"

GtkWidget*
create_Convert_window ()
{
  GtkWidget *vbox1;
  GtkWidget *frame1;
  GtkWidget *vbox2;
  GtkWidget *hbox1;
  GtkWidget *hbox2;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget* Convert_icon;
  GtkWidget *hbox3;
  GtkWidget *hbox4;
  GtkWidget *label3;
  GtkWidget *frame2;
  GtkWidget *hbuttonbox1;
  GtkWidget *combo_entry;
  GList *Convert_combo_items = NULL;
  
  Convert_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (Convert_window), "Convert_window", Convert_window);
  gtk_window_set_title (GTK_WINDOW (Convert_window), _("Convert File-types"));
  gtk_signal_connect (GTK_OBJECT (Convert_window), "destroy", GTK_SIGNAL_FUNC (Destroy_convert), NULL);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (Convert_window), vbox1);

  frame1 = gtk_frame_new (_("Convert files"));
  gtk_box_pack_start (GTK_BOX (vbox1), frame1, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame1), 3);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame1), vbox2);

  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox1, TRUE, TRUE, 0);

  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox1), hbox2, TRUE, TRUE, 0);

  label1 = gtk_label_new (_("Convert file"));
  gtk_box_pack_start (GTK_BOX (hbox2), label1, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (label1), GTK_JUSTIFY_LEFT);

  label2 = gtk_label_new (_("Filetype"));
  gtk_box_pack_start (GTK_BOX (hbox2), label2, FALSE, FALSE, 0);
  
  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, TRUE, TRUE, 0);

  ConvertFile_fileentry = gnome_file_entry_new (NULL, NULL);
  gtk_box_pack_start (GTK_BOX (hbox3), ConvertFile_fileentry, FALSE, TRUE, 0);

  combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (ConvertFile_fileentry));
  
  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox3), hbox4, FALSE, TRUE, 0);

  label3 = gtk_label_new (_("   To:   "));
  gtk_box_pack_start (GTK_BOX (hbox4), label3, FALSE, FALSE, 0);

  Convert_combo = gtk_combo_new ();
  Convert_combo_items = g_list_append (Convert_combo_items, _("Slackware (.tgz)"));
  Convert_combo_items = g_list_append (Convert_combo_items, _("Redhat (.rpm)"));
  Convert_combo_items = g_list_append (Convert_combo_items, _("Debian (.deb)"));
  gtk_combo_set_popdown_strings (GTK_COMBO (Convert_combo), Convert_combo_items);
  g_list_free (Convert_combo_items);
  gtk_box_pack_start (GTK_BOX (hbox4), Convert_combo, TRUE, TRUE, 0);

  Convert_combo_entry = GTK_COMBO (Convert_combo)->entry;
  gtk_entry_set_text (GTK_ENTRY (Convert_combo_entry), _("Slackware (.tgz)"));
  gtk_entry_set_editable (GTK_ENTRY (Convert_combo_entry), FALSE);

  frame2 = gtk_frame_new (_("Status"));
  gtk_box_pack_start (GTK_BOX (vbox1), frame2, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame2), 3);

  ConvertStatus_text = gtk_text_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (frame2), ConvertStatus_text);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox1, FALSE, TRUE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox1), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbuttonbox1), 0);

  Convert_button = gtk_button_new_with_label (_("Convert"));
  Convert_icon = gnome_stock_pixmap_widget (Convert_window, GNOME_STOCK_PIXMAP_CONVERT); 
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), Convert_button);
  GTK_WIDGET_SET_FLAGS (Convert_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (Convert_button), "clicked", GTK_SIGNAL_FUNC (on_Convert_activate), NULL);

  ConvertClose_button = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), ConvertClose_button);
  GTK_WIDGET_SET_FLAGS (ConvertClose_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (ConvertClose_button), "clicked", GTK_SIGNAL_FUNC (on_ConvertClose_activate), NULL);

  gtk_widget_show (vbox1);
  gtk_widget_show (frame1);
  gtk_widget_show (vbox2);
  gtk_widget_show (hbox1);
  gtk_widget_show (hbox2);
  gtk_widget_show (label1);
  gtk_widget_show (label2);
  gtk_widget_show (hbox3);
  gtk_widget_show (ConvertFile_fileentry);
  gtk_widget_show (Convert_combo_entry);
  gtk_widget_show (hbox4);
  gtk_widget_show (label3);
  gtk_widget_show (Convert_combo);
  gtk_widget_show (combo_entry);
  gtk_widget_show (frame2);
  gtk_widget_show (ConvertStatus_text);
  gtk_widget_show (hbuttonbox1);
  gtk_widget_show (Convert_button);
  gtk_widget_show (ConvertClose_button);

  return Convert_window;
}