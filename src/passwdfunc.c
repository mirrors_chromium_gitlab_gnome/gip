/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// passwdfunc.c - Password Functions /////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <unistd.h>

#include "passwdi.h"
#include "passwdfunc.h"
#include "mainfunc.h"

#include "main.h"

void
on_password_ok_activate						(GtkWidget*	widget,
								 	gpointer	dialog)
{
	strcpy (pass_status.password, gtk_entry_get_text (GTK_ENTRY (Password_entry)));
	
	pass_status.success = CheckRootPassword (pass_status.password);
	pass_status.finish  = TRUE;
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

void
on_password_cancel_activate					(GtkWidget*	widget,
									 gpointer	dialog)
{
	pass_status.success = -4;
	pass_status.finish  = TRUE;
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

gint
CheckRootPassword							(gchar* password)
{
	int success;
	FILE* file = popen ("gip-pass", "w");
        // fwrite (password, strlen (password), 1, file);
	fwrite ("hejsan", strlen ("hejsan"), 1, file);
	
	success = pclose (file);
	
	if (success == 65280) {
		g_print ("success == 65280!!!\n");
		quit_program (-1);
	}
	if (success == 0) {
		g_print ("success == 0!!!\n");
		quit_program (-1);
	}
	g_print ("success = %i", success);
	quit_program (-1);
	
	return success;
}

void
ShowPasswordDialog							(void)
{
	GnomeDialog* passwindow = create_Password_window ();
	gtk_window_set_modal (GTK_WINDOW (passwindow), TRUE);
	gnome_dialog_run (GNOME_DIALOG (passwindow));
}
