/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// softinsti.h - Software Installer Interface ////////////////////
/////////////////////////////////////////////////////////////////////////////

#include <gnome.h>

GtkWidget* InstallSoftware_window;

GtkWidget* Step1_window;
GtkWidget* Step2_window;
GtkWidget* Step3_window;
GtkWidget* Install_button;
GtkWidget* InstallFile_fileentry;
GtkWidget* InstallStatus_progressbar;
gboolean   storeglobal;
GtkWidget* InstallStatus_text;
gchar*     instFile;
GtkWidget* Install_label;
GtkWidget* InstallNext_button;
GtkWidget* Package_label;
GtkWidget* InstallProgram_label;
GtkWidget* Advanced_button;
GtkWidget* MakeExtra_entry;
GtkWidget* ConfExtra_entry;

GtkWidget* ViewReadme_checkbutton;
GtkWidget* ViewFAQ_checkbutton;
GtkWidget* ReturnToGip_checkbutton;
GtkWidget* RunProgram_checkbutton;

GtkWidget* Step1;
GtkWidget* Step2;
GtkWidget* Step3;

GtkWidget*
create_InstallSoftware_window ();
GtkWidget* 
create_Step1_window (void);
GtkWidget* 
create_Step2_window (void);
GtkWidget* 
create_Step3_window (void);