/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <math.h>

#include "converti.h"
#include "maini.h"
#include "mainfunc.h"
#include "softinsti.h"
#include "softuninsti.h"
#include "support.h"
#include "database.h"
#include "softinsti.h"
#include "softuninsti.h"
#include "main.h"

gchar* gip_running_file = NULL;
static gchar* geo_message = NULL;
static gboolean install = FALSE;
static gboolean uninstall = FALSE;


struct poptOption options[] = {
  {
	  "geometry",
	  '\0',
	  POPT_ARG_STRING,
	  &geo_message,
	  0,
	  N_("Specify the geometry of the main window"),
	  N_("GEOMETRY")
  },
  {
	  "install",
	  'i',
	  POPT_ARG_NONE,
	  &install,
	  0,
	  N_("Install file. If no file specified, the Installation dialog will open empty."),
	  NULL
  },
  {
	  "uninstall",
	  'u',
	  POPT_ARG_NONE,
	  &uninstall,
	  0,
	  N_("Uninstall files. If no files specified, the Uninstallation dialog will open empty."),
	  NULL
  },
  {
	  NULL,
	  '\0',
	  0,
	  NULL,
	  0,
	  NULL,
	  NULL
  }
};

void
quit_program (gint retv)
{
	DataBase_SaveAll();
     
	/* Remove gip_running_file before gip quits ------ */
	remove (gip_running_file);
	
	exit (retv);
}

void
segmentfault_handler(gint signal)
{
	DataBase_SaveAll ();
	
	/* Remove gip_running_file before gip quits ------  */
	remove (gip_running_file);
	
	exit (-1);
}

int
main (int argc, char *argv[])
{
	GtkWidget* MainWindow;
	GtkWidget* About;
	GtkWidget* mess;
	gint file;
	gint return_stat;
	struct stat mystats;
	gchar* local_gip_dir;
	gchar* homedir;
	poptContext pctx;
	gchar** args = NULL;

	/* Var init ---------------------------------------- */
	homedir = g_strdup (getenv ("HOME"));
	local_gip_dir = g_strconcat (homedir, "/.gip", NULL);
	gip_running_file = g_strconcat (homedir, "/.gip/gip_is_running", NULL);
  
	/* GNOME init -------------------------------------- */
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
	
	gnome_init_with_popt_table ("GIP", VERSION, argc, argv,
				    options, 0, &pctx);
	
	/* Initiate mwin structure ------------------------- */
	mwin.xpos = 0;
	mwin.ypos = 0;
	mwin.height = 600;
	mwin.width = 800;
	mwin.starttype = STARTMAIN;

	/* Popt stuff -------------------------------------- */
	args = (gchar**)  poptGetArgs (pctx);
	
	if (geo_message != NULL) {
		gnome_parse_geometry  (geo_message, &mwin.xpos, &mwin.ypos,
				       &mwin.width, &mwin.height);
	}

	if (install)
		mwin.starttype = STARTINSTALL;
	if (uninstall)
		mwin.starttype = STARTUNINSTALL;
	
	poptFreeContext (pctx);
	
	/* Segmentfaultsignal ----------------------------- */
	signal (SIGSEGV, segmentfault_handler);
	
	/* Check if gip_running_file exists. If true, then exit */
	if (g_file_exists (gip_running_file)) {
		mess = gnome_message_box_new (_("GIP is already running.\nYou may only have on gip running at the same time.
  							\n\nIf you're sure GIP is not running, then try to delete the file ~/.gip/gip_is_running\nand start GIP again."), GNOME_MESSAGE_BOX_INFO,
					      GNOME_STOCK_BUTTON_OK, NULL);
		gtk_window_set_modal (GTK_WINDOW (mess), TRUE);
		gnome_dialog_run (GNOME_DIALOG (mess));
		return -1;
	}
  
	/* Create gip_running_file ------------------------- */
	file = open (gip_running_file, O_CREAT | O_TRUNC);
	close (file);
	
	/* Check and create .gip directory ----------------- */
	if (!stat(local_gip_dir, &mystats)) {		/* Exists */
		if (!S_ISDIR(mystats.st_mode)) {		/* ~/.gip exists but is NOT a directory */
			fprintf(stderr, _("main: %s is not a directory. Remove it.\n"), local_gip_dir);
			exit(1);
		}
	}
	else if (errno == ENOENT)			/* It doesn't exist */
		mkdir(local_gip_dir, S_IRUSR | S_IWUSR | S_IXUSR);		/* we can handle this */
	else {
		perror(_("main: Checking YOUR local GIP directory"));
		exit(1);
	}
	
	/* Initiate database ------------------------------- */
	DataBase_Init();
	

	/* Start Main, Install or Uninstall window ----- --- */
	if (mwin.starttype == STARTINSTALL) {
		MainWindow = create_InstallSoftware_window ();
		gtk_widget_show (MainWindow);
		gtk_signal_connect (GTK_OBJECT (MainWindow), "destroy", GTK_SIGNAL_FUNC (quit_program), NULL);
	}
	else if (mwin.starttype == STARTUNINSTALL) {
		MainWindow = create_Uninstall_window ();
		gtk_widget_show (MainWindow);
		gtk_signal_connect (GTK_OBJECT (MainWindow), "destroy", GTK_SIGNAL_FUNC (quit_program), NULL);
	}
	else {
		MainWindow = create_MainWindow ();
		gtk_widget_show (MainWindow);
		gtk_widget_set_sensitive (GTK_WIDGET (Preferences_button), FALSE);
		gtk_signal_connect (GTK_OBJECT (MainWindow), "destroy", GTK_SIGNAL_FUNC (quit_program), NULL);
	}
	
	init_variables ();
	
	gtk_main ();      
	
	DataBase_SaveAll();
	
	/* Remove gip_running_file before quit ---------- */
	remove (gip_running_file);   
	
	return 0;
}








