/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// softinstfunc.h - Software Installer Functions /////////////////
/////////////////////////////////////////////////////////////////////////////

#include <gnome.h>

gboolean next2shown;
gboolean next3shown;
gboolean instshown;

/* Declarate som  variables */
gchar* InstallDir;
gchar* InstallFile;
gchar* TempDir;
gchar* TempFile;
gchar* ExecFile;
gchar* FinishFile;
gchar* SuccessFile;
gint   InstType;
gpointer Widgets;
	
enum Work { prepare, copy, uncompress, saveR, saveF,
			configure, compile, global, clean };

struct AdvOpt {
	gchar* ConfigureExtraOptions;
	gchar* MakeExtraOptions;
	gchar* RpmInstallExtraOptions;
	gchar* RpmUpgradeExtraOptions;
	gchar* RpmUninstallExtraOptions;
	gchar* DebInstallExtraOptions;
	gchar* DebUninstallExtraOptions;
} AdvancedOptions;
	
/* Internal funktions used in GIP_init () */
void SetInstallDir  		(gchar* dir);
void SetInstallFile 		(gchar* file);
void SetTempDir	    		(gchar* dir);
void SetTempFile    		(gchar* file);
void SetExecFile    		(gchar* file);
void SetFinishFile              (gchar* file);
void SetSuccessFile             (gchar* file);
void SetInstType    		(gint insttype);

/* Internal funktions for Actions */
gboolean CreateExec 	 	(gint work);
gboolean RunExec	    	(gint work);
gboolean CleanUpExec 		(void);

/* Check Status */
gboolean CheckIfInstalled	(void);
gboolean CheckIfSuccess		(void);

/* Get info from directory */
gboolean GetDirInfo (const gchar* dir);

/* Init the GIPInstall structure */
void GIP_init (gchar* installdir, gchar* tempdir,
	       gchar* tempfile, gchar* execfile,
	       gchar* finfile, gchar* succfile,
	       gint insttype, gpointer widgets);

/* Public funktions used for Installation */
gboolean PrepareInstallation 	(void);
gboolean CopyTempFileToTempDir  (void);
gboolean UncompressFile		(void);
gboolean SaveReadme		(void);
gboolean SaveFaq		(void);
gboolean RunConfigure		(void);
gboolean Compile		(void);
gboolean InstallGlobal		(void);
gboolean CleanUp		(void);
void	 Finish			(void); 	


gboolean 
Cancel_Inst 							(GnomeDruidPage *page,
								 gpointer 		 arg1,
								 gpointer 		 user_data);
gboolean
Destroy_Install							(GtkWidget*		widget,
								 gpointer		user_data);

gboolean
on_next1_activate						(GnomeDruidPage *druidpage,
								 gpointer		 arg1,
								 gpointer		 user_data);          						 								 

void
on_storeglobal_radiobutton_activate				(GtkWidget		*widget,
								 gpointer		 user_data);

void
on_storelocal_radiobutton_activate				(GtkWidget		*widget,
								 gpointer		 user_data);

void
preparePrepareGipInstall					(GtkWidget		*widget,
								 gpointer		 druid);
							 			
void
GIP_Install 							(gpointer		druid);

void
InstallationFinished						(GnomeDruidPage* druidpage,
								 gpointer		 arg1,
								 gpointer		 window);

void
on_saveinstfile_yes_radiobutton_activate 			(GtkWidget		*widget,
								  gpointer		 user_data);
							
void
on_saveinstfile_no_radiobutton_activate				(GtkWidget		*widget,
								 			 gpointer		 user_data);
