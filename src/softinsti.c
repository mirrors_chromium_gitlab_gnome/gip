/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/
 
/////////////////////////////////////////////////////////////////////////////
///////////// softinsti.c - Software Installer Interface ////////////////////
/////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "softinsti.h"
#include "softinstfunc.h"
#include "maini.h"
#include "mainfunc.h"
#include "support.h"
#include "advancedi.h"
#include "advancedfunc.h"

GtkWidget*
create_InstallSoftware_window ()
{
  GtkWidget* Welcome_druidpagestart;
  GtkWidget* BasicConf_druidpagestandard;
  GtkWidget* druid_vbox1;
  GtkWidget* vbox1;
  GtkWidget* fixed1;
  GtkWidget* viewport1;
  GtkWidget* pixmap1;
  GtkWidget* label2;
  GtkWidget* InstallFile_combo_entry;
  GtkWidget* hseparator1;
  GtkWidget* label3;
  GSList*    fixed1_group = NULL;
  GtkWidget* hseparator2;
  GtkWidget* label4;
  GtkWidget* Installing_druidpagestandard;
  GtkWidget* druid_vbox2;
  GtkWidget* fixed3;
  GtkWidget* viewport4;
  GtkWidget* pixmap4;
  GtkWidget* label5;
  GtkWidget* hseparator3;
  GtkWidget* InstallComplete_druidpagestandard;
  GtkWidget* druid_vbox3;
  GtkWidget* fixed2;
  GtkWidget* viewport3;
  GtkWidget* pixmap3;
  GSList*    fixed2_group = NULL;
  GtkWidget* KeepInstFile_radiobutton;
  GtkWidget* DontKeepInstFile_radiobutton;
  GtkWidget* hseparator4;
  GtkWidget* label8;
  GtkWidget* hseparator5;
  GtkWidget* label9;
  GtkWidget* label7;
  GtkWidget* Bye_druidpagefinish;
  GtkWidget* Install_druid;
  GtkWidget* InstallGlobal_radiobutton;
  GtkWidget* InstallLocal_radiobutton;

  AdvancedOptions.ConfigureExtraOptions    = NULL;
  AdvancedOptions.MakeExtraOptions         = NULL;
  AdvancedOptions.RpmInstallExtraOptions   = NULL;
  AdvancedOptions.RpmUninstallExtraOptions = NULL;
  AdvancedOptions.DebInstallExtraOptions   = NULL;
  AdvancedOptions.DebUninstallExtraOptions = NULL;

  InstallSoftware_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (InstallSoftware_window), "InstallSoftware_window", InstallSoftware_window);
  gtk_window_set_title (GTK_WINDOW (InstallSoftware_window), _("Install/Upgrade Software"));
  gtk_signal_connect (GTK_OBJECT (InstallSoftware_window), "destroy", GTK_SIGNAL_FUNC (Destroy_Install), NULL);
  
  Install_druid = gnome_druid_new ();
  gtk_container_add (GTK_CONTAINER (InstallSoftware_window), Install_druid);

  Welcome_druidpagestart = gnome_druid_page_start_new ();
  gnome_druid_append_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (Welcome_druidpagestart));
  gnome_druid_set_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (Welcome_druidpagestart));
  gnome_druid_page_start_set_title (GNOME_DRUID_PAGE_START (Welcome_druidpagestart), _("Welcome to GIP"));
  gnome_druid_page_start_set_text (GNOME_DRUID_PAGE_START (Welcome_druidpagestart), _("Welcome to GIP, the GNOME Software Installer/Uninstaller.\n\nThis is guide will help you install and upgrade software.\nPress next to continue."));
  gnome_druid_page_start_set_logo (GNOME_DRUID_PAGE_START (Welcome_druidpagestart),
                                   create_image ("gip.xpm"));
  gtk_signal_connect (GTK_OBJECT (Install_druid), "cancel", GTK_SIGNAL_FUNC (Cancel_Inst), NULL);

  BasicConf_druidpagestandard = gnome_druid_page_standard_new_with_vals ("", NULL);
  gtk_widget_show_all (BasicConf_druidpagestandard);
  gnome_druid_append_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (BasicConf_druidpagestandard));
  gnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (BasicConf_druidpagestandard), _("Step 1/3 - Basic configuration"));
  gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (BasicConf_druidpagestandard),
                                      create_image ("gip.xpm"));
  gtk_signal_connect (GTK_OBJECT (BasicConf_druidpagestandard), "next", GTK_SIGNAL_FUNC (on_next1_activate), NULL);

  druid_vbox1 = GNOME_DRUID_PAGE_STANDARD (BasicConf_druidpagestandard)->vbox;
  
  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (druid_vbox1), vbox1, TRUE, TRUE, 0);

  fixed1 = gtk_fixed_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), fixed1, TRUE, TRUE, 0);

  viewport1 = gtk_viewport_new (NULL, NULL);
  gtk_fixed_put (GTK_FIXED (fixed1), viewport1, 8, 8);
  gtk_widget_set_uposition (viewport1, 8, 8);
  gtk_widget_set_usize (viewport1, 96, 250);

  pixmap1 = create_pixmap (InstallSoftware_window, "gip/gip-install.xpm", FALSE);
  gtk_container_add (GTK_CONTAINER (viewport1), pixmap1);
  gtk_widget_set_usize (pixmap1, 96, 250);

  Advanced_button = gtk_button_new_with_label (_("Advanced..."));
  gtk_fixed_put (GTK_FIXED (fixed1), Advanced_button, 120, 176);
  gtk_widget_set_uposition (Advanced_button, 120, 176);
  gtk_widget_set_usize (Advanced_button, 88, 24);
  gtk_signal_connect (GTK_OBJECT (Advanced_button), "clicked", GTK_SIGNAL_FUNC (on_advancedbutton_activate), NULL);
  gtk_widget_set_sensitive (GTK_WIDGET (Advanced_button), FALSE);

  label2 = gtk_label_new (_("Select Installation file:"));
  gtk_fixed_put (GTK_FIXED (fixed1), label2, 120, 16);
  gtk_widget_set_uposition (label2, 120, 16);
  gtk_widget_set_usize (label2, 360, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label2), TRUE);

  InstallFile_fileentry = gnome_file_entry_new (NULL, NULL);
  gtk_fixed_put (GTK_FIXED (fixed1), InstallFile_fileentry, 120, 40);
  gtk_widget_set_uposition (InstallFile_fileentry, 120, 40);
  gtk_widget_set_usize (InstallFile_fileentry, 368, 24);
 
  InstallFile_combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (InstallFile_fileentry));
  hseparator1 = gtk_hseparator_new ();
  gtk_fixed_put (GTK_FIXED (fixed1), hseparator1, 120, 72);
  gtk_widget_set_uposition (hseparator1, 120, 72);
  gtk_widget_set_usize (hseparator1, 368, 16);
   gtk_signal_connect (GTK_OBJECT (InstallFile_combo_entry), "changed", GTK_SIGNAL_FUNC (on_installfilentrychanged_activate), InstallFile_combo_entry);

  label3 = gtk_label_new (_("How would you like to store the software?"));
  gtk_fixed_put (GTK_FIXED (fixed1), label3, 120, 88);
  gtk_widget_set_uposition (label3, 120, 88);
  gtk_widget_set_usize (label3, 360, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label3), TRUE);

  InstallGlobal_radiobutton = gtk_radio_button_new_with_label (fixed1_group, _("Global (requires root password)"));
  fixed1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (InstallGlobal_radiobutton));
  gtk_fixed_put (GTK_FIXED (fixed1), InstallGlobal_radiobutton, 128, 112);
  gtk_widget_set_uposition (InstallGlobal_radiobutton, 128, 112);
  gtk_widget_set_usize (InstallGlobal_radiobutton, 240, 24);
  gtk_signal_connect (GTK_OBJECT (InstallGlobal_radiobutton), "clicked", GTK_SIGNAL_FUNC (on_storeglobal_radiobutton_activate), NULL);

  InstallLocal_radiobutton = gtk_radio_button_new_with_label (fixed1_group, _("Local"));
  fixed1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (InstallLocal_radiobutton));
  gtk_fixed_put (GTK_FIXED (fixed1), InstallLocal_radiobutton, 128, 136);
  gtk_widget_set_uposition (InstallLocal_radiobutton, 128, 136);
  gtk_widget_set_usize (InstallLocal_radiobutton, 240, 24);
  gtk_signal_connect (GTK_OBJECT (InstallLocal_radiobutton), "clicked", GTK_SIGNAL_FUNC (on_storelocal_radiobutton_activate), NULL);

  hseparator2 = gtk_hseparator_new ();
  gtk_fixed_put (GTK_FIXED (fixed1), hseparator2, 120, 160);
  gtk_widget_set_uposition (hseparator2, 120, 160);
  gtk_widget_set_usize (hseparator2, 368, 16);

  label4 = gtk_label_new (_("Press next to continue. \nThe software will now be installed."));
  gtk_fixed_put (GTK_FIXED (fixed1), label4, 120, 208);
  gtk_widget_set_uposition (label4, 120, 208);
  gtk_widget_set_usize (label4, 368, 40);
  gtk_label_set_justify (GTK_LABEL (label4), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label4), TRUE);

  Installing_druidpagestandard = gnome_druid_page_standard_new_with_vals ("", NULL);
  gnome_druid_append_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (Installing_druidpagestandard));
  gnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (Installing_druidpagestandard), _("Step 2/3 - Installing Software..."));
  gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (Installing_druidpagestandard),
                                      create_image ("gip.xpm"));
  gtk_signal_connect (GTK_OBJECT (Installing_druidpagestandard), "map", GTK_SIGNAL_FUNC (preparePrepareGipInstall), Install_druid);

  druid_vbox2 = GNOME_DRUID_PAGE_STANDARD (Installing_druidpagestandard)->vbox;
  
  fixed3 = gtk_fixed_new ();
  gtk_box_pack_start (GTK_BOX (druid_vbox2), fixed3, TRUE, TRUE, 0);

  InstallStatus_progressbar = gtk_progress_bar_new ();
  gtk_progress_set_show_text (GTK_PROGRESS (InstallStatus_progressbar), TRUE);
  gtk_fixed_put (GTK_FIXED (fixed3), InstallStatus_progressbar, 120, 32);
  gtk_widget_set_uposition (InstallStatus_progressbar, 120, 32);
  gtk_widget_set_usize (InstallStatus_progressbar, 368, 24);

  InstallStatus_text = gtk_text_new (NULL, NULL);
  gtk_fixed_put (GTK_FIXED (fixed3), InstallStatus_text, 120, 96);
  gtk_widget_set_uposition (InstallStatus_text, 120, 96);
  gtk_widget_set_usize (InstallStatus_text, 368, 161);

  viewport4 = gtk_viewport_new (NULL, NULL);
  gtk_fixed_put (GTK_FIXED (fixed3), viewport4, 8, 8);
  gtk_widget_set_uposition (viewport4, 8, 8);
  gtk_widget_set_usize (viewport4, 96, 250);

  pixmap4 = create_pixmap (InstallSoftware_window, "gip/gip-install.xpm", FALSE);
  gtk_container_add (GTK_CONTAINER (viewport4), pixmap4);
  gtk_widget_set_usize (pixmap4, 96, 250);

  label5 = gtk_label_new (_("Installation Status:"));
  gtk_fixed_put (GTK_FIXED (fixed3), label5, 120, 16);
  gtk_widget_set_uposition (label5, 120, 16);
  gtk_widget_set_usize (label5, 352, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label5), TRUE);

  Package_label = gtk_label_new (_(""));
  gtk_fixed_put (GTK_FIXED (fixed3), Package_label, 120, 80);
  gtk_widget_set_uposition (Package_label, 120, 80);
  gtk_widget_set_usize (Package_label, 352, 16);
  gtk_label_set_line_wrap (GTK_LABEL (Package_label), TRUE);

  hseparator3 = gtk_hseparator_new ();
  gtk_fixed_put (GTK_FIXED (fixed3), hseparator3, 120, 64);
  gtk_widget_set_uposition (hseparator3, 120, 64);
  gtk_widget_set_usize (hseparator3, 368, 16);

  InstallComplete_druidpagestandard = gnome_druid_page_standard_new_with_vals ("", NULL);
  gnome_druid_append_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (InstallComplete_druidpagestandard));
  gnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (InstallComplete_druidpagestandard), _("Step 3/3 - Installation Completed!"));
  gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (InstallComplete_druidpagestandard),
                                      create_image ("gip.xpm"));

  druid_vbox3 = GNOME_DRUID_PAGE_STANDARD (InstallComplete_druidpagestandard)->vbox;
  fixed2 = gtk_fixed_new ();
  gtk_box_pack_start (GTK_BOX (druid_vbox3), fixed2, TRUE, TRUE, 0);

  viewport3 = gtk_viewport_new (NULL, NULL);
  gtk_fixed_put (GTK_FIXED (fixed2), viewport3, 8, 8);
  gtk_widget_set_uposition (viewport3, 8, 8);
  gtk_widget_set_usize (viewport3, 96, 250);

  pixmap3 = create_pixmap (InstallSoftware_window, "gip/gip-install.xpm", FALSE);
  gtk_container_add (GTK_CONTAINER (viewport3), pixmap3);
  gtk_widget_set_usize (pixmap3, 96, 250);

  ViewReadme_checkbutton = gtk_check_button_new_with_label (_("View Readme"));
  gtk_fixed_put (GTK_FIXED (fixed2), ViewReadme_checkbutton, 264, 208);
  gtk_widget_set_uposition (ViewReadme_checkbutton, 264, 208);
  gtk_widget_set_usize (ViewReadme_checkbutton, 104, 24);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ViewReadme_checkbutton), TRUE);

  ViewFAQ_checkbutton = gtk_check_button_new_with_label (_("View FAQ"));
  gtk_fixed_put (GTK_FIXED (fixed2), ViewFAQ_checkbutton, 376, 208);
  gtk_widget_set_uposition (ViewFAQ_checkbutton, 376, 208);
  gtk_widget_set_usize (ViewFAQ_checkbutton, 112, 24);

  KeepInstFile_radiobutton = gtk_radio_button_new_with_label (fixed2_group, _("Yeah (Recommended)"));
  fixed2_group = gtk_radio_button_group (GTK_RADIO_BUTTON (KeepInstFile_radiobutton));
  gtk_fixed_put (GTK_FIXED (fixed2), KeepInstFile_radiobutton, 128, 40);
  gtk_widget_set_uposition (KeepInstFile_radiobutton, 128, 40);
  gtk_widget_set_usize (KeepInstFile_radiobutton, 300, 24);
  gtk_signal_connect (GTK_OBJECT (KeepInstFile_radiobutton), "clicked", GTK_SIGNAL_FUNC (on_saveinstfile_yes_radiobutton_activate), NULL);

  DontKeepInstFile_radiobutton = gtk_radio_button_new_with_label (fixed2_group, _("No (the original file will not be deleted)"));
  fixed2_group = gtk_radio_button_group (GTK_RADIO_BUTTON (DontKeepInstFile_radiobutton));
  gtk_fixed_put (GTK_FIXED (fixed2), DontKeepInstFile_radiobutton, 128, 64);
  gtk_widget_set_uposition (DontKeepInstFile_radiobutton, 128, 64);
  gtk_widget_set_usize (DontKeepInstFile_radiobutton, 300, 24);
  gtk_signal_connect (GTK_OBJECT (DontKeepInstFile_radiobutton), "clicked", GTK_SIGNAL_FUNC (on_saveinstfile_no_radiobutton_activate), NULL);

  hseparator4 = gtk_hseparator_new ();
  gtk_fixed_put (GTK_FIXED (fixed2), hseparator4, 120, 96);
  gtk_widget_set_uposition (hseparator4, 120, 96);
  gtk_widget_set_usize (hseparator4, 368, 16);

  label8 = gtk_label_new (_("Would you like to return to GIP main window?"));
  gtk_fixed_put (GTK_FIXED (fixed2), label8, 120, 120);
  gtk_widget_set_uposition (label8, 120, 120);
  gtk_widget_set_usize (label8, 368, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label8), TRUE);

  ReturnToGip_checkbutton = gtk_check_button_new_with_label (_("Yes"));
  gtk_fixed_put (GTK_FIXED (fixed2), ReturnToGip_checkbutton, 120, 136);
  gtk_widget_set_uposition (ReturnToGip_checkbutton, 120, 136);
  gtk_widget_set_usize (ReturnToGip_checkbutton, 99, 24);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ReturnToGip_checkbutton), TRUE);

  hseparator5 = gtk_hseparator_new ();
  gtk_fixed_put (GTK_FIXED (fixed2), hseparator5, 120, 160);
  gtk_widget_set_uposition (hseparator5, 120, 160);
  gtk_widget_set_usize (hseparator5, 368, 16);

  label9 = gtk_label_new (_("The End"));
  gtk_fixed_put (GTK_FIXED (fixed2), label9, 120, 184);
  gtk_widget_set_uposition (label9, 120, 184);
  gtk_widget_set_usize (label9, 368, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label9), TRUE);

  RunProgram_checkbutton = gtk_check_button_new_with_label (_("Run the program"));
  gtk_fixed_put (GTK_FIXED (fixed2), RunProgram_checkbutton, 128, 208);
  gtk_widget_set_uposition (RunProgram_checkbutton, 128, 208);
  gtk_widget_set_usize (RunProgram_checkbutton, 120, 24);
  gtk_widget_set_sensitive (GTK_WIDGET (RunProgram_checkbutton), FALSE);

  label7 = gtk_label_new (_("Do you want to keep the installaton file?"));
  gtk_fixed_put (GTK_FIXED (fixed2), label7, 120, 24);
  gtk_widget_set_uposition (label7, 120, 24);
  gtk_widget_set_usize (label7, 380, 16);
  gtk_label_set_line_wrap (GTK_LABEL (label7), TRUE);

  Bye_druidpagefinish = gnome_druid_page_finish_new ();
  gnome_druid_append_page (GNOME_DRUID (Install_druid), GNOME_DRUID_PAGE (Bye_druidpagefinish));
  gnome_druid_page_finish_set_title (GNOME_DRUID_PAGE_FINISH (Bye_druidpagefinish), _("Congratulations - Installation succeeded"));
  gnome_druid_page_finish_set_text (GNOME_DRUID_PAGE_FINISH (Bye_druidpagefinish), _("<Software> Installed successfully."));
  gnome_druid_page_finish_set_logo (GNOME_DRUID_PAGE_FINISH (Bye_druidpagefinish),
                                   create_image ("gip.xpm"));
  gtk_signal_connect (GTK_OBJECT (Bye_druidpagefinish), "finish", GTK_SIGNAL_FUNC (InstallationFinished), InstallSoftware_window);

  gtk_widget_show (Install_druid);
  gtk_widget_show (Welcome_druidpagestart);
  gtk_widget_show (druid_vbox1);
  gtk_widget_show (vbox1);
  gtk_widget_show (fixed1);
  gtk_widget_show (viewport1);
  gtk_widget_show (pixmap1);
  gtk_widget_show (Advanced_button);
  gtk_widget_show (label2);
  gtk_widget_show (InstallFile_fileentry);
  gtk_widget_show (InstallFile_combo_entry);
  gtk_widget_show (hseparator1);
  gtk_widget_show (label3);
  gtk_widget_show (InstallGlobal_radiobutton);
  gtk_widget_show (InstallLocal_radiobutton);
  gtk_widget_show (hseparator2);
  gtk_widget_show (label4);
  gtk_widget_show (Installing_druidpagestandard);
  gtk_widget_show (druid_vbox2);
  gtk_widget_show (fixed3);
  gtk_widget_show (InstallStatus_progressbar);
  gtk_widget_show (InstallStatus_text);
  gtk_widget_show (viewport4);
  gtk_widget_show (pixmap4);
  gtk_widget_show (label5);
  gtk_widget_show (Package_label);
  gtk_widget_show (hseparator3);
  gtk_widget_show (InstallComplete_druidpagestandard);
  gtk_widget_show (druid_vbox3);
  gtk_widget_show (fixed2);
  gtk_widget_show (viewport3);
  gtk_widget_show (pixmap3);
  gtk_widget_show (ViewReadme_checkbutton);
  gtk_widget_show (ViewFAQ_checkbutton);
  gtk_widget_show (KeepInstFile_radiobutton);
  gtk_widget_show (DontKeepInstFile_radiobutton);
  gtk_widget_show (hseparator4);
  gtk_widget_show (label8);
  gtk_widget_show (ReturnToGip_checkbutton);
  gtk_widget_show (hseparator5);
  gtk_widget_show (label9);
  gtk_widget_show (RunProgram_checkbutton);
  gtk_widget_show (label7);
  gtk_widget_show (Bye_druidpagefinish);

  return InstallSoftware_window;
}
