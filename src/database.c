/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/*
   MARMORAT Irwin mirwinm@yahoo.com
   All rights reserved (1999)

   ?.?.99 : begin write this file
   30.10.99 : correct bug with allocation : replace sizeof by strlen -> it works !!!
              add DataBase_Util_GetNameVersion(); and GetFormat
	      add format,filename,version in structure

              bug : when open a new database , name and version got wrong caratere ...
	            -> corrected : (* xx)[end]='\0';

   31.10.99:  rename database_util_getnameversion by database_util_getinfo
              do a lot on DataBase_Util_GetInfo : now catch path into string
	      add file size detection
	      add time management
	      no bug for the moment : it's wonderfull !!!
	       bad , i found one : reading data installation time in file return wrong date ... CORRECTED

    (near midnight, even if my eyes were closed ...) add DataBase_RegisterSoftware() 

    1.11.99 : add database_x_file;
              add all this file to support 3 database (install,uninstall,db)

    2.11.99 : i do a lot, change all this file to work with glib function (g_slist_xxx)
              add DataBase_Unregister(xx,xx)

    4.11.99 : make error message clearest
              improve behaviour (sometime strange char appears in path and name string)
	       by modifing reading functions.
	       now all is ok :)
              split installation into 2 parts : before and after ...

 */

#include <sys/stat.h>
#include <ctype.h>
#include "database.h"

extern GtkWidget * inst_list;
extern GtkWidget * uninst_list;


FILE * fdb;
char gip_actual_db_version;

char gip_type[3][9] = { "Unknown ","Gnome","Kde" };

// gip_format : it's un classified and not used for the moment
char gip_format[12][10] = { "tar",
                          "tar.gz",
			  "tgz",
			  "tar.bz",
			  "tar.bz2",
			  "Z",
			  "zip",
			  "bz",
			  "bz2",
			  "rpm",
			  "rpms",
                          "unknown"}; 

Db_File DB[2];

// ********************************
// * Initialisation function      *
// ********************************

void
DataBase_Init()
{
  gchar * gip_dir = g_strdup_printf("%s/.gip",getenv("HOME"));
  fdb = 0;

  DB [GIP_DB_INSTALL_ID  ].filename = g_strdup_printf("%s/.gip/%s",getenv("HOME"),GIP_DB_INSTALL_FILE);
  DB [GIP_DB_UNINSTALL_ID].filename = g_strdup_printf("%s/.gip/%s",getenv("HOME"),GIP_DB_UNINSTALL_FILE);
  
  DB [GIP_DB_INSTALL_ID  ].db = (GSList *)NULL;
  DB [GIP_DB_UNINSTALL_ID].db = (GSList *)NULL;
  
  if (!g_file_exists(gip_dir))
    mkdir(gip_dir,0666);
 
  gip_actual_db_version = (char) GIP_DB_VERSION1;
}

// ******************************
// * Installation functions     *
// ******************************

DataBase
DataBase_InstallEntry_Start(DataBase item,char *filename,int type,int option)
{
  char *name, *version, *path;
  int  format=0,size;
  struct stat *buf =(struct stat *)g_malloc(sizeof(struct stat));
  struct tm *t_local;
  time_t t;

  t = time(NULL);
  t_local = localtime(&t);
  
  // thanks rhult and boc for help me :) 
  if (stat(filename,buf) == -1)
      printf("Can't get %s file size\n",filename);

  DataBase_Util_GetInfo(filename,&name,&path,&version,&format);
 
  item = g_new(struct TDataBase,1); 
  g_assert(item != NULL);
  
  item->name = g_strdup(name);
  item->path = g_strdup(path);
  item->version  = g_strdup(version);
  item->filename = g_strdup(filename);
  item->comment  = g_strdup("no comment");
  item->author   = g_strdup("?");
  item->app_type = g_strdup("Unknown"); // information will be overwrited (if .gip exists)
  item->exec     = g_strdup(name);

  item->format = (char)format; 
  item->type   = (char)type;
  item->option = (char)option;
  item->size      = (long)buf->st_size;
  item->time.hour = (char)t_local->tm_hour;
  item->time.min  = (char)t_local->tm_min;
  item->time.mon  = (char)t_local->tm_mon;
  item->time.day  = (char)t_local->tm_mday;
  item->time.year = (char)t_local->tm_year;
  
  item->next = (DataBase)NULL;
  item->desktop_files = (GSList *)NULL;
  item->directory_files = (GSList *)NULL;
  
  if (g_file_exists("/tmp/gip/.gip"))
    item->has_gip_spec_file = TRUE;
  else
    item->has_gip_spec_file = FALSE;
 
  /* for further use
  if (gip_spec_file_load(".gip",item) != 0)
    printf("error while reading file\n");
  */

  return item;
}

void
DataBase_InstallEntry_Finalize(DataBase item)
{
  DataBase_RegisterSoftware(item);
  DB[GIP_DB_INSTALL_ID].db = g_slist_append(DB[GIP_DB_INSTALL_ID].db,(DataBase)item);
  DataBase_UpdateAllView();
}

void
DataBase_UninstallEntry(int item)
{
  DataBase db_item = NULL;
  
  if (item == -1)
    return;

  db_item = (DataBase) g_slist_nth_data(DB[GIP_DB_INSTALL_ID].db,item); 
  g_assert(db_item != NULL);
  printf("uninstall software %s\n",db_item->name);
  DataBase_UnregisterSoftware((char *)NULL,db_item->name);
  DB[GIP_DB_UNINSTALL_ID].db = (GSList *)g_list_append(DB[GIP_DB_UNINSTALL_ID].db,(DataBase)db_item);
  DB[GIP_DB_INSTALL_ID].db =   (GSList *)g_slist_remove(DB[GIP_DB_INSTALL_ID].db,(DataBase)db_item);
  DataBase_UpdateAllView();
}

void
DataBase_AddEntry(int id,char *filename,
                         char *name,
			 char *path,
			 char *version,
			 char *comment,
			 char *app_type,
			 char *author,
			 char *exec,
			 int type,
                         int option,
			 int format,
			 long size,
			 int hour,
			 int min,
			 int mon,
			 int day,
			 int year)
{
  int mem;
  DataBase item;
  
  item = g_new(struct TDataBase,1); 
  g_assert(item != NULL);
  
  item->name = g_strdup(name);
  item->path = g_strdup(path);
  item->version  = g_strdup(version);
  item->filename = g_strdup(filename);
  item->comment  = g_strdup(comment);
  item->app_type = g_strdup(app_type);
  item->author   = g_strdup(author);
  item->exec     = g_strdup(exec);
  
  item->format = (char)format; 
  item->type   = (char)type;
  item->option = (char)option;
  item->size      = size;
  item->time.hour = (char)hour;
  item->time.min  = (char)min;
  item->time.mon  = (char)mon;
  item->time.day  = (char)day;
  item->time.year = (char)year;
  item->next = NULL;

  DB[id].db = (GSList *)g_slist_append(DB[id].db,(DataBase)item);
}

// ****************************************
// * Reading functions                    *
// ****************************************

void 
DataBase_ReadAll()
{
  DataBase_Read(GIP_DB_INSTALL_ID);
  DataBase_Read(GIP_DB_UNINSTALL_ID);
}

void 
DataBase_Read(int id)
{
  char gip_db_version;
  GtkWidget * mess;
  gchar * error_mess;
  
  char *name,*path,*version,*filename,*comment,*app_type,*author,*exec;
  char tmp;
  char option,type,hour,day,min,year,mon,format;
  guint size;
  int nbread;
  
  fdb = fopen(DB[id].filename,"r+");
  
  if (fdb == NULL)
  {
    // we don't say to the user that the database doesn't exist
    // because new one will be created when quitting the soft. 
    
    /*
    error_mess = g_strdup_printf("GIP: can't open database '%s'\nCreate new one",DB[id].filename);
    mess = gnome_message_box_new(error_mess,
	                         GNOME_MESSAGE_BOX_WARNING,
	                         GNOME_STOCK_BUTTON_OK,
				 NULL);
    gtk_widget_show(mess);
    */
    return;
  }
  else 
    fscanf(fdb,"%c",&gip_db_version);

#ifdef DEBUG
  printf("DataBase -> inbuilt version %c\n",gip_actual_db_version);
  printf("DataBase -> database version %c\n",gip_db_version);
#endif

  switch(gip_db_version)
  {
    case GIP_DB_VERSION1 : 
                           while(!feof(fdb))
			   {
			     fscanf(fdb,"%c",&tmp);
			     if (feof(fdb)) break;
			     
			     name = (gchar *)g_malloc(tmp+1);
			     fread(name,tmp,1,fdb);

			     fscanf(fdb,"%c",&tmp);
			     path = (gchar *)g_malloc(tmp+1);
			     fread(path,tmp,1,fdb);


			     fscanf(fdb,"%c",&tmp);
                             version = (gchar *)g_malloc(tmp);
			     fread(version,tmp,1,fdb);
		        
			     fscanf(fdb,"%c",&tmp);
                             filename = (gchar *)g_malloc(tmp);
			     fread(filename,tmp,1,fdb);

			     fscanf(fdb,"%c",&tmp);
                             comment = (gchar *)g_malloc(tmp);
			     fread(comment,tmp,1,fdb);
			
     			     fscanf(fdb,"%c",&tmp);
                             app_type = (gchar *)g_malloc(tmp);
			     fread(app_type,tmp,1,fdb);
	
			     fscanf(fdb,"%c",&tmp);
                             author = (gchar *)g_malloc(tmp);
			     fread(author,tmp,1,fdb);
	   	             
			     fscanf(fdb,"%c",&tmp);
                             exec = (gchar *)g_malloc(tmp);
			     fread(exec,tmp,1,fdb);
	
			     fscanf(fdb,"%c%c%c",&format,&type,&option);
			     fscanf(fdb,"%c%c%c%c%c",&hour,&min,&mon,&day,&year);
			     fscanf(fdb,"%8x",&size);	

			     DataBase_AddEntry(id,filename,
				                  name,
						  path,
						  version,
						  comment,
						  app_type,
						  author,
						  exec,
						  type,
						  option,
						  format,
						  size,
						  hour,
						  min,
						  mon,
						  day,
						  year);
			   }
                           break;
                default  : mess = gnome_message_box_new("Unknown type of database",GNOME_MESSAGE_BOX_ERROR,
			                                GNOME_STOCK_BUTTON_OK,NULL);
			   gtk_widget_show(mess);
			   return;
  }
  //DataBase_Close();
#ifdef DEBUG
  printf("DataBase -> reading done\n");
#endif
}

void
DataBase_SaveAll()
{
  DataBase_Save(GIP_DB_INSTALL_ID);
  DataBase_Save(GIP_DB_UNINSTALL_ID);
}

void
DataBase_SaveItem(DataBase item,FILE * fdb)
{
    fprintf(fdb,"%c%s",strlen(item->name)+1,item->name);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->path)+1,item->path);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->version)+1,item->version);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->filename)+1,item->filename);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->comment)+1,item->comment);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->app_type)+1,item->app_type);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->author)+1,item->author);
    fputc('\x00',fdb);
    fprintf(fdb,"%c%s",strlen(item->exec)+1,item->exec);
    fputc('\x00',fdb);
    fprintf(fdb,"%c",item->format);
    fprintf(fdb,"%c%c",item->type,item->option);
    fprintf(fdb,"%c%c%c%c%c",item->time.hour,item->time.min,item->time.mon,item->time.day,item->time.year);
    fprintf(fdb,"%8x",item->size);
}

void
DataBase_Save(int id)
{
  FILE * fdb;
  GtkWidget * mess;
  gchar * error_mess;
  gchar * gip_dir = g_strdup_printf("%s/.gip",getenv("HOME"));
  
  if (g_file_exists(gip_dir))
    mkdir(gip_dir,0666);
  
  fdb = fopen(DB[id].filename,"w");
  if (fdb == NULL)
  {
      error_mess = g_strdup_printf("GIP ALERT: Can't save database '%s'\n",DB[id].filename);
      mess = gnome_message_box_new(error_mess,
	                           GNOME_MESSAGE_BOX_ERROR,
                                   GNOME_STOCK_BUTTON_OK,NULL);
      gtk_widget_show(mess);
      return;

   } 
 
  fprintf(fdb,"%c",gip_actual_db_version);
  g_slist_foreach(DB[id].db,(GFunc)DataBase_SaveItem,(FILE *)fdb);
  
  //DataBase_Close();
  //printf("DataBase -> %s saved\n",DB[id].filename);
}


void
DataBase_Close()
{
  fclose(fdb);
}

// ****************************************
// * Test function                        *
// ****************************************
/* for further use

void 
DataBase_Test_Gip_Spec()
{
  FILE * fp = fopen(".gip","w+");
  fprintf(fp,"gip_spec_ver=1\n");
  fprintf(fp,"name = \"Gnome Ftp\"\n");
  fprintf(fp,"version = \"1.1.1\"\n");
  fprintf(fp,"type = \"Internet\"\n");
  fprintf(fp,"comment = \"Ftp file manager \"\n");
  fprintf(fp,"author = \"Bart\"\n");
  fclose(fp);
}

void DataBase_Test()
{
  DataBase item;
  DataBase_Test_Gip_Spec();  
  item = DataBase_InstallEntry_Start(item,"/root/gftp-1.0.tar.gz",GIP_TYPE_GNOME,0);
  DataBase_InstallEntry_Finalize(item);
}
*/

// ******************************
// * View functions             *
// ******************************

void 
DataBase_Install_View_AddItem(DataBase item,gpointer user_data)
{
  gchar strsize[10];
  gchar * buffer[6];
  
  sprintf(strsize,"%d",item->size / 1024);

  buffer[0] = (char *)item->name;    //"software"
  buffer[1] = (char *)item->version; //"version"
  buffer[2] = (char *)strsize;       //"size"
  buffer[3] = (char *)gip_type[(int)item->type]; //"GNOME/KDE"
  buffer[4] = (char *)item->comment;        //"description"
  buffer[5] = (char *)NULL;
	
  gtk_clist_append(GTK_CLIST (inst_list),buffer);
}

void
DataBase_Uninstall_View_AddItem(DataBase item,gpointer user_data)
{
  char * buffer[5];
 
  buffer[0] = (char *)item->name;    //"software"
  buffer[1] = (char *)item->version; //"version"
  buffer[2] = (char *)gip_type[(int)item->type]; //"GNOME/KDE"
  buffer[3] = (char *)"not implemented"; //"description"
  buffer[4] = (char *)NULL;
	
  gtk_clist_append(GTK_CLIST (uninst_list),buffer);
}

void
DataBase_UpdateAllView()
{
#ifdef DEBUG
  printf("DataBase -> Updating View List\n");
#endif
  gtk_clist_clear (GTK_CLIST (inst_list));
  gtk_clist_clear (GTK_CLIST (uninst_list));
  gtk_clist_freeze(GTK_CLIST (inst_list));
  gtk_clist_freeze(GTK_CLIST (uninst_list));

  g_slist_foreach(DB[GIP_DB_INSTALL_ID].db,  (GFunc)DataBase_Install_View_AddItem,NULL);
  g_slist_foreach(DB[GIP_DB_UNINSTALL_ID].db,(GFunc)DataBase_Uninstall_View_AddItem,NULL);

  gtk_clist_thaw  (GTK_CLIST (inst_list));
  gtk_clist_thaw  (GTK_CLIST (uninst_list));
}

void
DataBase_Util_Show_Info(guint item)
{
  DataBase db_entry;
  db_entry = g_slist_nth_data(DB[GIP_DB_INSTALL_ID].db,item);
  if (db_entry == NULL)
  {
    printf("this item doesn't exists !!! there is a bug \n");
    return;
  }
  else
  {
    printf("****************************\n");
    printf("name = %s\n",db_entry->name);
    printf("exec = %s\n",db_entry->exec);
    printf("version = %s\n",db_entry->version);
    printf("author = %s\n",db_entry->author);
    printf("app_type = %s\n",db_entry->app_type);
    printf("filename = %s\n",db_entry->filename);
    printf("size = %ld\n",db_entry->size);
    printf("comment = %s\n",db_entry->comment);
    printf("path = %s\n",db_entry->path);
    printf("date = %d/%d/%d %d:%d\n",db_entry->time.day,
	                             db_entry->time.mon,
				     db_entry->time.year,
				     db_entry->time.hour,
				     db_entry->time.min);
    printf("****************************\n");
  }
}

// *******************************************
// * Some utils fonctions                    *
// *******************************************


gchar *
DataBase_Get_Name_From_Entry_Num(guint item)
{
  // just used for uninstall section
  DataBase db_entry = g_slist_nth_data(DB[GIP_DB_INSTALL_ID].db,item);
  if (db_entry != NULL)
    return db_entry->filename;
  else
    return NULL;
}
      
int
DataBase_Util_GetFormat(char * string)
{
  int i = GIP_FORMAT_UNKNOWN;
  
  for (i=0;i<NB_FORMAT;i++)
  {
    if (strcmp(string,gip_format[i]) == 0)
      return i;
  }
  return i;
}

void
DataBase_Util_GetInfo(char * soft,char ** name,char ** path,char ** version,int * format)
{
  /*
     important note : this routine extract name and version only with this patterns : name-version.format
     if can't find the version , return '?'
     if can't find the name , return soft value;

     may be in future , we can have name and version of the software in GIP specification file ...

     note : now accept pattern like name.format
  */

  int i=0;
  int pos;
    
  char * temp;

  // new : extract path (because instFile has it)
  temp = strchr(soft,'/');
  if (temp != NULL) // ok it has the path
  {
    temp = strrchr(soft,'/'); // search last '/'
    (* path) = g_strndup(soft,temp-soft+1);
    soft = soft + (temp - soft) + 1;
  }

  
  temp = strrchr(soft,'-');
  if (temp == NULL)
  {
    // pattern name-version.format don't match -> try name.format
 
    (* version) = "?";
    (* format)  = GIP_FORMAT_UNKNOWN;
    temp = strchr(soft,'.');
    if (temp == NULL)
    {
      (* name) = g_strdup(soft);
      printf("Can't resolv name in string %s \n",soft);
    }
    else
    {
      (* name ) = g_strndup(soft,temp-soft);
      temp++;
      (* format) = DataBase_Util_GetFormat(temp);
      printf("FIXME : when not version, can even take format \n");
    }
    
    return;
  }
  (* name) = g_strndup(soft,temp-soft);
  
  for (i=1;i<=strlen(temp);i++) //we begin with i=1 caus at temp[1]='-';
  {
    if (isalpha(temp[i]) != 0) //found a [A..Z] or [a..z]
    {
      pos = i-1;
      temp ++;
      (* version)=g_strndup(temp,pos-1);
      
      temp = temp + pos;
      (* format) = DataBase_Util_GetFormat(temp);
      return; 
    }
  
  }
  
}

// ***************************************************************************
// *         registration software part                                      *
// ***************************************************************************

void
DataBase_RegisterSoftware_Write_Default_Directory_File(gchar * dir)
{
  FILE * fp;
  gchar * destdir;
  gchar * directory_file;
 
  destdir = g_strdup_printf("%s/%s",GNOME_APPS_DIR,dir);
  directory_file = g_strdup_printf("%s/.directory",destdir);

  if (g_file_exists(directory_file))
    return;
  
  if (!g_file_exists(destdir))
    mkdir(destdir,0666);
 
  // write default entry 
  fp = fopen(directory_file,"a");
  g_assert(fp != NULL);
  fprintf(fp,"[Desktop Entry]\n"); 
  fprintf(fp,"Name=GIP Installed Software\n");
  fprintf(fp,"Comment=Software installed from GIP\n");
  fprintf(fp,"Icon=gnome-folder.png\n");
  fprintf(fp,"Type=Directory");
  fclose(fp);
}
 
void
DataBase_RegisterSoftware_Write_Default_Desktop_File(gchar * entry,DataBase db_entry)
{
  FILE * fp = fopen(entry,"a");
  g_assert(fp != NULL);
  fprintf(fp,"[Desktop Entry]\n");
  fprintf(fp,"Name=%s\n",db_entry->name);
  fprintf(fp,"Comment=%s\n",db_entry->comment);
  fprintf(fp,"Exec=%s\n",db_entry->exec);
  fprintf(fp,"Icon=gnome-default.png\n");
  fprintf(fp,"Terminal=0\n");
  fprintf(fp,"Type=Unknown");
  fclose(fp);
} 

void
DataBase_RegisterSoftware_Default(DataBase dbentry)
{
  gchar * GnomeSection;
  gchar * DirectoryFile;
  gchar * EntryFile;
  gchar * section;
  FILE  * fp;
  GtkWidget * mess;
  gchar * error_mess;
  
  section = "Install";
  GnomeSection = g_strdup_printf("%s/%s",GNOME_APPS_DIR,section);
  DirectoryFile = g_strdup_printf("%s/.directory",GnomeSection);
  EntryFile = g_strdup_printf("%s/%s.desktop",GnomeSection,dbentry->name);
  
  if (!g_file_exists(GnomeSection))
    mkdir(GnomeSection,0666); // if directory don't exists , create it
  
  if (!g_file_exists(DirectoryFile))
    DataBase_RegisterSoftware_Write_Default_Directory_File(section);

  if (!g_file_exists(EntryFile))
    DataBase_RegisterSoftware_Write_Default_Desktop_File(EntryFile,dbentry);
  else
  {
    error_mess = g_strdup_printf("Gnome register file : a program with name %s\nalready  exists !!",dbentry->name);
    
    mess = gnome_message_box_new(error_mess,
	                         GNOME_MESSAGE_BOX_ERROR,
				 GNOME_STOCK_BUTTON_OK,
				 NULL);
    gtk_widget_show(mess);
    return;
  }

}

void
DataBase_RegisterSoftware(DataBase dbentry)
{
  if (dbentry->has_gip_spec_file != TRUE)
    DataBase_RegisterSoftware_Default(dbentry);
  // else we do nothing because make install will install automaticaly desktop files.
}

void
DataBase_UnregisterSoftware(char * section, char * appname)
{
  gchar * GnomeSection;
  gchar * EntryFile;
  FILE *fp;
  GtkWidget *mess;
  gchar * error_mess;
 
  if (section == NULL) section = "Install";
  
  GnomeSection = g_strdup_printf("%s/%s",GNOME_APPS_DIR,section);
  EntryFile = g_strdup_printf("%s/%s.desktop",GnomeSection,appname);

  if (remove(EntryFile) != 0)
  {
    error_mess = g_strdup_printf("Gnome unregister file: can't remove item\n'%s' in gnome menu",appname);
    mess = gnome_message_box_new(error_mess,
	                         GNOME_MESSAGE_BOX_ERROR,
				 GNOME_STOCK_BUTTON_OK,
				 NULL);
    gtk_widget_show(mess);
    return;
  }

}
