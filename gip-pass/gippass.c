/*
 * GIP. A Software Manager Tool.
 *
 * Copyright (C) 1999, 2000 :
 * Andreas Hyden <andreas.hyden@telia.com>
 * and
 * Marmorat Irwin <mirwinm@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

/////////////////////////////////////////////////////////////////////////////
///////////// gippass.c /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <shadow.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

#include "gippass.h"

int
CheckRootPasswd (char* password)
{
	char* correct_password;
	char* encrypt_password;
	int success;
	
	/* Get root's password (encrypted) */
	struct spwd *sp = getspnam ("root");
	
	/* Encrypt entered password */
	encrypt_password = (char *) crypt (password, "ab");
	
	if (sp) {
		correct_password = malloc ((size_t) strlen ((char *) sp->sp_pwdp));
		strcpy (correct_password, (char *) sp->sp_pwdp);
	}
	
	/* Does given password match with root's ? */
	success = (!strcmp (password, correct_password));
	
	endpwent ();
	free (correct_password);
	
	return success;
}

int
RunCommandAsRoot (char* password)
{
	struct stat execfile;
	if (!CheckRootPasswd (password)) {	/* Check if entered password is correct */
		if (!stat ("/tmp/gip/exec", &execfile)) {	/* File /tmp/gip/exec exists */
			system ("bash /tmp/gip/exec &");
			return 0;
		}
		else /* File doesn't exist */
			return -2;
	}
	return -1;
}

int main (void)
{
	char* indata;
	FILE *file = fdopen (STDIN_FILENO, "r");
	int success;
	
	indata = malloc (100);
	fgets (indata, 100, file);
	
	pclose (file);
	
	success = (RunCommandAsRoot((char* ) indata));
	
	return success;
}